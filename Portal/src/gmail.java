import com.sun.xml.internal.stream.util.ThreadLocalBufferAllocator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class gmail {
    public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {
        int total = 0;
        PrintWriter writer = new PrintWriter("UNREAD.txt", "UTF-8");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\Documents\\" +
                "Academics\\Software Engineering\\My Materials\\Third Year 2 Semester\\SE\\Lab\\chromedriver.exe");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("test-type");
        options.addArguments("--start-maximized");
        options.addArguments("--disable-web-security");
        options.addArguments("--allow-running-insecure-content");
        capabilities.setCapability("chrome.binary","./src//lib//chromedriver");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        WebDriver driver = new ChromeDriver();
      driver.manage().window().maximize();
        String appUrl = "https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession";

        driver.get(appUrl);
        driver.findElement(By.id("identifierId")).sendKeys("hanatesfaye223@gmail.com", Keys.ENTER);
        Thread.sleep(4000);
        driver.findElement(By.name("password")).sendKeys(" Enter password here: ", Keys.ENTER);
        Thread.sleep(4000);

        List<WebElement> unread_email = driver.findElements(By.xpath("//*[@class='zA zE']"));
        for (int i = 0; i < unread_email.size(); i++) {
            if (!unread_email.get(i).getText().equals("")) {
                total += 1;
                writer.println("You have an unread email from: " + unread_email.get(i).getText());
            }
        }

        writer.println("Total unread emails: " + total);
        writer.close();
        driver.close();

    }

}
