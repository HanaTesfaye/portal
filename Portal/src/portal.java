import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.PrintWriter;

public class portal {
    public static void main(String[] args)throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\Documents\\" +
                "Academics\\Software Engineering\\My Materials\\Third Year 2 Semester\\SE\\Lab\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        String appUrl = "http://portal.aait.edu.et";
        driver.get(appUrl);
        String gradeUrl = "https://portal.aait.edu.et/Grade/GradeReport";
        driver.findElement(By.xpath(".//*[@id=\"UserName\"]")).sendKeys("ATR/4224/09");
        driver.findElement(By.xpath(".//*[@id=\"Password\"]")).sendKeys("Enter password here: ");
        driver.findElement(By.xpath(".//*[@id=\"home\"]/div[2]/div[2]/form/div[4]/div/button")).click();
        Thread.sleep(100);
        driver.navigate().to(gradeUrl);
        String gradeOutput = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div")).getText();
        System.out.println(gradeOutput);
        //Blank Document
        try{
            PrintWriter writer = new PrintWriter("gradefile.txt", "UTF-8");
            writer.println(gradeOutput);
            writer.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        Thread.sleep(5000000);
        driver.close();
    }
}
